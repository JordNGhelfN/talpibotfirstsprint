from django.contrib import admin
from django.urls import path
import login.login_app.views as views
import numpy as np

urlpatterns = [
    path('', views.login_page, name='login'),
    path('login/', views.get_login, name='login'),
    # path('try_login/', actions.try_login, name='try_login'),
    path('register_page/', views.try_register, name='try_reg'),
    path('home1/', views.home1, name='home1'),
    path('home2/', views.home2, name='home2'),
    # path('logout/', actions.logout, name='logout'),
    path('try_register/', views.register, name='register')
]


