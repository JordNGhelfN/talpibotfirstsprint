from django.http import request
from django.shortcuts import redirect, render
from django.shortcuts import HttpResponseRedirect

usernames = []
mails = []
names = []
passwords = []

data = [usernames, mails, names, passwords]


def login_page(request):
    return render(request, 'login_page.html')


def try_register(request):
    return render(request, 'register_page.html')


def home1(request):
    return render(request, 'home1.html')


def home2(request):
    return render(request, 'home2.html')


def register(request):
    username = request.POST['username']
    mail = "abc@abc.abc"
    name = request.POST['name']
    password = request.POST['password']

    if check_if_username_available(username):
        res = create_user(username, mail, name, password)
        if res == -1:
            response = "/register_page/?msg=register-failed"
            return HttpResponseRedirect(response)
        response = f"/?msg=success"
        return HttpResponseRedirect(response)
    else:
        return redirect("/register_page/?msg=user-already-registered")


def get_login(request: request):
    username = request.POST['username']
    password = request.POST['password']

    # print(f"username = {username}, password = {password}")
    # print(usernames)

    if login(username, password) == 1:
        request.session['username'] = username
        return redirect(f"/home1/?msg=success&name={names[get_username_index(username)]}")
    else:
        return redirect("/?msg=login-failed")


def check_if_user_exist(username):
    global data
    return username in data[0]


def login(username, password):
    global data
    if check_if_user_exist(username):
        index = data[0].index(username)
        if data[3][index] == password:
            return True
    return False


def check_if_username_available(username):
    global data
    if username in data[0]:
        return False
    return True


def create_user(username, mail, name, password):
    global data
    if not check_if_user_exist(username):
        data[0].append(username)
        data[1].append(mail)
        data[2].append(name)
        data[3].append(password)
        return 1
    else:
        return -1


def change_password(username, new_password):
    global data
    if check_if_user_exist(username):
        index = data[0].index(username)
        data[3][index] = new_password
        return True
    return False


def get_name(username):
    global data
    if check_if_user_exist(username):
        index = data[0].index(username)
        return data[2][index]
    return False


def to_string(username, mail, name):
    string = f"Hi {name}! \n Your username: {username}!\n Your email: {mail}!"
    return string


def get_username_index(username):
    return data[0].index(username)


def get_detailes_by_username(username):
    global data
    index = get_username_index(username)
    return to_string(username, data[1][index], data[2][index])


def is_valid_password(password: str):
    flag_alphas = False
    flag_nums = False
    flag_special = False

    if len(password) < 8:
        return False
    for char in password:
        if char.isalpha():
            flag_alphas = True
        elif char.isdigit():
            flag_nums = True
        elif char in "!@#$%^&*()?~_+<>:":
            flag_special = True
    return flag_special and flag_nums and flag_alphas
